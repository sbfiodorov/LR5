import org.junit.jupiter.api.Assertions;
import java.util.*;
import java.util.stream.Stream;

class ShapeCollectorTest
{
    @org.junit.jupiter.api.Test
    void findMinArea()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.stream().min(Comparator.comparing(Shape::figure)).ifPresent(System.out::println);
        Assertions.assertEquals
                (
                        "Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }",
                        "Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }"
                );
    }

    @org.junit.jupiter.api.Test
    void findMaxArea()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.stream().max(Comparator.comparing(Shape::figure)).ifPresent(System.out::println);
        Assertions.assertEquals
                (
                        "Circle{radius=7.0, fillColor=YELLOW_GREEN, borderColor=BROWN}",
                        "Circle{radius=7.0, fillColor=YELLOW_GREEN, borderColor=BROWN}"
                );
    }

    @org.junit.jupiter.api.Test
    void allFiguresSquare()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));

        double allArea = 0;
        for (Shape figure : figures)
        {
            allArea += figure.area();
        }
        System.out.println(allArea);

        Assertions.assertEquals(allArea, 162.93804002589985);
    }

    @org.junit.jupiter.api.Test
    void findByFillColor()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.fillColor() == Color.AQUAMARINE).forEach(System.out::println);
        Assertions.assertEquals(Color.AQUAMARINE, Color.AQUAMARINE);
    }

    @org.junit.jupiter.api.Test
    void findByBorderColor()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.borderColor() == Color.LIGHT_GREEN).forEach(System.out::println);
        Assertions.assertEquals(Color.LIGHT_GREEN, Color.LIGHT_GREEN);
    }

    @org.junit.jupiter.api.Test
    void printFigures()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        figures.forEach(System.out::println);
        Assertions.assertEquals(3, 3);
    }

    @org.junit.jupiter.api.Test
    void printCountFigures()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        System.out.println(figures.size());
        Assertions.assertEquals(3, 3);
    }

    @org.junit.jupiter.api.Test
    void figuresForFillColor()
    {
        List<Shape> figures = new ArrayList<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        Set<Color> keys = new HashSet<>();
        keys.add(Color.AQUAMARINE);

        for (Color key : keys)
        {
            System.out.println(key + " : ");
        }
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.fillColor() == Color.AQUAMARINE).forEach(System.out::println);
        Assertions.assertEquals
                ("AQUAMARINE :[Circle{radius=5.0, fillColor=AQUAMARINE, borderColor=MAGENTA}, Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }, Triangle { ground = 8.0, height = 4.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]\n" +
                        "YELLOW :[Circle{radius=7.0, fillColor=YELLOW, borderColor=BROWN}]\n" +
                        "BLUE_STEEL :[Rectangle { width = 10.0, height = 12.0, fillColor = BLUE_STEEL, borderColor = LIGHT_GREEN }]", "AQUAMARINE :[Circle{radius=5.0, fillColor=AQUAMARINE, borderColor=MAGENTA}, Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }, Triangle { ground = 8.0, height = 4.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]\n" +
                        "YELLOW :[Circle{radius=7.0, fillColor=YELLOW, borderColor=BROWN}]\n" +
                        "BLUE_STEEL :[Rectangle { width = 10.0, height = 12.0, fillColor = BLUE_STEEL, borderColor = LIGHT_GREEN }]"
                );
    }

    @org.junit.jupiter.api.Test
    void figuresForBorderColor()
    {
        List<Shape> figures = new ArrayList<>();
        Map<Color, Figures> map1 = new HashMap<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        map1.put(Color.LIGHT_GREEN, new Figures());
        Set<Color> keys = map1.keySet();

        for (Color key : keys)
        {
            System.out.println(key + " : ");
        }
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.borderColor() == Color.LIGHT_GREEN).forEach(System.out::println);
        Assertions.assertEquals
                ("MAGENTA :[Circle{radius=5.0, fillColor=AQUAMARINE, borderColor=MAGENTA}]\n" +
                        "BROWN :[Circle{radius=7.0, fillColor=YELLOW, borderColor=BROWN}]\n" +
                        "LIGHT_GREEN :[Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }, Rectangle { width = 10.0, height = 12.0, fillColor = BLUE_STEEL, borderColor = LIGHT_GREEN }, Triangle { ground = 8.0, height = 4.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]", "MAGENTA :[Circle{radius=5.0, fillColor=AQUAMARINE, borderColor=MAGENTA}]\n" +
                        "BROWN :[Circle{radius=7.0, fillColor=YELLOW, borderColor=BROWN}]\n" +
                        "LIGHT_GREEN :[Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }, Rectangle { width = 10.0, height = 12.0, fillColor = BLUE_STEEL, borderColor = LIGHT_GREEN }, Triangle { ground = 8.0, height = 4.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]"
                );
    }

    @org.junit.jupiter.api.Test
    void figuresForType()
    {
        List<Shape> figures = new ArrayList<>();
        Map<Color, Figures> map1 = new HashMap<>();
        figures.add(new Circle(7, Color.YELLOW_GREEN, Color.BROWN, FiguresTypes.CIRCLE));
        figures.add(new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE));
        figures.add(new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE));
        map1.put(Color.LIGHT_GREEN, new Figures());
        Set<Color> keys = map1.keySet();

        for (Color key : keys)
        {
            System.out.println(key + " : ");
        }
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.borderColor() == Color.LIGHT_GREEN).forEach(System.out::println);
        Assertions.assertEquals
                ("TRIANGLE :[Triangle { ground = 8.0, height = 4.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]\n" +
                        "RECTANGLE :[Rectangle { width = 10.0, height = 12.0, fillColor = BLUE_STEEL, borderColor = LIGHT_GREEN }]\n" +
                        "SQUARE :[Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]\n" +
                        "CIRCLE :[Circle{radius=5.0, fillColor=AQUAMARINE, borderColor=MAGENTA}, Circle{radius=7.0, fillColor=YELLOW, borderColor=BROWN}]", "TRIANGLE :[Triangle { ground = 8.0, height = 4.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]\n" +
                        "RECTANGLE :[Rectangle { width = 10.0, height = 12.0, fillColor = BLUE_STEEL, borderColor = LIGHT_GREEN }]\n" +
                        "SQUARE :[Square { side = 3.0, fillColor = AQUAMARINE, borderColor = LIGHT_GREEN }]\n" +
                        "CIRCLE :[Circle{radius=5.0, fillColor=AQUAMARINE, borderColor=MAGENTA}, Circle{radius=7.0, fillColor=YELLOW, borderColor=BROWN}]"
                );
    }
}