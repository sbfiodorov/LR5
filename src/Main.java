import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        ShapeCollector<Shape> collector = new ShapeCollector<>();
        HashMap<Color, ArrayList<Shape>> map = new HashMap<>();
        HashMap<Color, ArrayList<Shape>> map1 = new HashMap<>();
        HashMap<FiguresTypes, ArrayList<Shape>> map2 = new HashMap<>();

        List<Shape> figures = new ArrayList<>();
        Circle circle = new Circle(5, Color.AQUAMARINE, Color.MAGENTA, FiguresTypes.CIRCLE);
        Circle circle1 = new Circle(7, Color.YELLOW, Color.BROWN, FiguresTypes.CIRCLE);
        Square square = new Square(3, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.SQUARE);
        Rectangle rectangle = new Rectangle(10, 12, Color.BLUE_STEEL, Color.LIGHT_GREEN, FiguresTypes.RECTANGLE);
        Triangle triangle = new Triangle(8, 4, Color.AQUAMARINE, Color.LIGHT_GREEN, FiguresTypes.TRIANGLE);

        collector.add(circle);
        collector.add(circle1);
        collector.add(square);
        collector.add(rectangle);
        collector.add(triangle);

        figures.add(circle);
        figures.add(circle1);
        figures.add(square);
        figures.add(rectangle);
        figures.add(triangle);

        for (Shape figure : figures)
        {
            System.out.println();
            System.out.println("Фигура : " + figure.figure());
            System.out.println("Площадь : " + figure.area());
            System.out.println("Цвет заливки : " + figure.fillColor());
            System.out.println("Цвет границы : " + figure.borderColor());
            System.out.println();
        }

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        System.out.println("Фигура с минимальной площадью, из всех имеющихся : " + collector.findMinArea(figures));
        System.out.println("Фигура с максимальной площадью, из всех имеющихся : " + collector.findMaxArea(figures));
        System.out.println("\nФигуры, которые имеются в списке : ");
        collector.printFigures(figures);
        System.out.println();
        System.out.println("Кол-во имеющихся фигур : " + collector.printCountFigures(figures));
        System.out.println("Суммарная площадь всех фигур : " + collector.allFiguresSquare(figures) + "\n");
        System.out.println("Поиск всех фигур по цвету заливки : ");
        collector.findByFillColor(figures);
        System.out.println();
        System.out.println("Поиск всех фигур по цвету границы : ");
        collector.findByBorderColor(figures);
        System.out.println("\nФигуры, сгруппированные по цвету заливки : ");
        collector.figuresForFillColor(map, figures);
        System.out.println("\nФигуры, сгруппированные по цвету границы : ");
        collector.figuresForBorderColor(map1, figures);
        System.out.println("\nФигуры, сгруппированные по определённому типу : ");
        collector.figuresForType(map2, figures);

        System.out.println();
        List<Comparable> e = new ArrayList<>();
        e.add(circle);
        e.add(circle1);
        e.add(square);
        e.add(rectangle);
        e.add(triangle);
        System.out.println(collector.getSorted(e));
    }
}