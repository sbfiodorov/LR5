import java.util.*;
import java.util.stream.Stream;

class ShapeCollector<T extends Shape>
{
    public List<T> list = new ArrayList<>();

    T findMinArea(List<T> figures)
    {
        return figures.stream().min(Comparator.comparing(T::area)).orElse(null);
    }

    T findMaxArea(List<T> figures)
    {
        return figures.stream().max(Comparator.comparing(T::area)).orElse(null);
    }

    double allFiguresSquare(List<T> figures)
    {
        double allArea = 0;
        for (T figure : figures)
        {
            allArea += figure.area();
        }
        return allArea;
    }

    void findByFillColor(List<T> figures)
    {
        Stream<T> stream = figures.stream();
        stream.filter(s -> s.fillColor() == Color.AQUAMARINE).forEach(System.out::println);
    }

    void findByBorderColor(List<T> figures)
    {
        Stream<T> stream = figures.stream();
        stream.filter(s -> s.borderColor() == Color.LIGHT_GREEN).forEach(System.out::println);
    }

    void printFigures(List<T> figures)
    {
        figures.forEach(System.out::println);
    }

    int printCountFigures(List<T> figures)
    {
        return figures.size();
    }

    void figuresForFillColor(Map<Color, ArrayList<T>> map, List<T> figures)
    {
        for (T shape : figures)
        {
            if (map.containsKey(shape.fillColor()))
            {
                map.get(shape.fillColor()).add(shape);
            }
            else
            {
                map.put(shape.fillColor(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }

        for (Map.Entry<Color, ArrayList<T>> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }

    void figuresForBorderColor(HashMap<Color, ArrayList<T>> map, List<T> figures)
    {
        for (T shape : figures)
        {
            if (map.containsKey(shape.borderColor()))
            {
                map.get(shape.borderColor()).add(shape);
            }
            else
            {
                map.put(shape.borderColor(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }

        for (Map.Entry<Color, ArrayList<T>> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }

    void figuresForType(HashMap<FiguresTypes, ArrayList<T>> map, List<T> figures)
    {
        for (T shape : figures)
        {
            if (map.containsKey(shape.figuresType()))
            {
                map.get(shape.figuresType()).add(shape);
            }
            else
            {
                map.put(shape.figuresType(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }

        for (Map.Entry<FiguresTypes, ArrayList<T>> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }

    public void add(T shape)
    {
        list.add(shape);
    }

    public List<T> addAll(List<T> lists)
    {
        list.addAll(lists);
        return list;
    }

    public List<Comparable> getSorted(List<Comparable> a)
    {
        a.sort(Comparable::compareTo);
        return a;
    }
}